#! /bin/bash -e

#echo "mergeTestOutput.sh - You must comment out this line!"; exit 1

if [ "$1" == "" ]; then
  thePlatforms=$BINARY_TAG
else
  thePlatforms="$*"
fi
echo "Platforms: '${thePlatforms}'"

theAwkDir=`dirname $0`
theAwkDir=`cd $theAwkDir; pwd`
theInDir1=$theAwkDir
theInDir2=$theAwkDir/new
theOutDir=$theAwkDir/new.merged
if [ ! -d ${theInDir1} ]; then
  echo "ERROR! Directory ${theInDir1} not found"
  exit 1
fi
if [ ! -d ${theInDir2} ]; then
  echo "ERROR! Directory ${theInDir2} not found"
  exit 1
fi
echo "Merged logfiles will be in ${theOutDir}" 
rm -rf ${theOutDir}
mkdir ${theOutDir}
for thePlatform in ${thePlatforms}; do
  if [ "${thePlatform}" != "" ]; then
    thePlatform=`basename ${thePlatform} .qmr`
    theFile=${thePlatform}.xml
    echo "******************************************************************" 
    echo "Process ${theFile}" 
    theInput1=${theInDir1}/${theFile}
    theInput2=${theInDir2}/${theFile}
    thePlatformOK=1
    if [ ! -f ${theInput1} ]; then
      echo "ERROR! File ${theInput1} not found"
      thePlatformOK=0
    fi
    echo "Merge ${theInput2} into ${theInput1}"
    if [ ! -f ${theInput2} ]; then
      echo "ERROR! File ${theInput2} not found"
      thePlatformOK=0
    fi
    if [ ${thePlatformOK} == 1 ]; then
      theOutput=${theOutDir}/${theFile}
      export AWKPATH=${theAwkDir}
      # Rename .CMT logfiles as standard logfiles
      ###\mv ${theInput2} ${theOutput}
      # Merge parts of the new logfiles into the old logfiles
      theTests2=`cat ${theInput2} | grep '<result id=' | awk '{print substr($2,5,length($2)-5)}'`
      theTests2=`echo ${theTests2}` # HACK to avoid newlines on Mac
      awk -v infile=1 -v input2=${theInput2} -v tests2="${theTests2}" -f mergeTestOutput.awk ${theInput1} > ${theOutput}
      # HACK (instead of fixing awk) for bug when rerunning/merging last test
      grep "</results>" ${theOutput} > /dev/null 2>&1
      if [ "$?" != "0" ]; then
        echo "</results>\n</report>" >> ${theOutput}
      fi
      echo "Produced merged file ${theOutput}" 
    fi
  fi
done
