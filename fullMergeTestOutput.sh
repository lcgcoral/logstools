#! /bin/bash -e

###echo "fullMergeTestOutput.sh - You must comment out this line!"; exit 1
 
if [ "$1" == "" ]; then
  thePlatforms=$BINARY_TAG
else
  thePlatforms="$*"
fi
echo "Platforms: '${thePlatforms}'"

# Remove the arguments - otherwise they confuse CMT setup.sh
while [ "$1" != "" ]; do shift; done

# Locate the log/qmtest directory
logQmtest=`dirname ${0}`
pushd $logQmtest > /dev/null 2>&1

# Summarize (qmr->xml), merge (xml->xml) and summarize again (xml->summary)
./summarizeAll.sh "${thePlatforms}"
\rm -rf new*
mkdir new
for thePlatform in ${thePlatforms}; do
  thePlatform=`basename ${thePlatform} .qmr`
  if [ "${thePlatform}" != "" ]; then
    ###echo "Platform: '${thePlatform}'"
    \mv ${thePlatform}.* new
  fi
done
svn update
./mergeTestOutput.sh ${thePlatforms}
if [ "$?" != "0" ]; then
  echo "ERROR! ./mergeTestOutput.sh failed!"
  exit 1
fi
\mv new.merged/* .
./summarizeAll.sh "${thePlatforms}"
###./performanceReport.sh "${thePlatforms}"

popd > /dev/null 2>&1

