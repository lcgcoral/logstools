#! /bin/bash

# NB! This script is best called from within the cc-sh shell,
# where both QMTEST_CLASS_PATH and CORALSYS/COOLSYS are defined

# Locate the qmr log file directory (i.e. this directory)
theQmrDir=`dirname ${0}`
theQmrDir=`cd ${theQmrDir}; pwd`

# Is this CORAL or COOL
if [ `basename ${theQmrDir}` == "qmtestCoral" ]; then
  theProjSys=CORALSYS
elif [ `basename ${theQmrDir}` == "qmtestCool" ]; then
  theProjSys=COOLSYS
else
  echo "ERROR! Unknown (old-style?) qmtest logs directory"
  exit 1
fi

# Check arguments (use CMT? hidden parameter, do not even document it!)
useCmake=1
if [ "$1" == "-cmt" ]; then
  useCmake=0
  shift
fi

# Check arguments
if [ "$1" == "-h" ]; then
  echo "Usage: `basename ${0}` [file1[.qmr] [file2[.qmr]]... ]"
  exit 1
elif [ "$1" != "" ]; then
  thePlatforms="$*"
  shift $#
elif [ "$BINARY_TAG" != "" ] && [ "$useCmake" != "0" ]; then
  thePlatforms=$BINARY_TAG
elif [ "$CMTCONFIG" != "" ] && [ "$useCmake" == "0" ]; then
  thePlatforms=$CMTCONFIG
else 
  if [ "$useCmake" != "0" ]; then
    echo "ERROR! No argument given but BINARY_TAG is not defined"
  else
    echo "ERROR! No argument given but CMTCONFIG is not defined"
  fi
  exit 1
fi

# For CMT: global setup assuming hardcoded paths to CORALSYS/COOLSYS
# (no support for executing tests from release areas with logs in /tmp)
# [For CMake: different setup for each logfile depending on CORALSYS/COOLSYS]
if [ "$useCmake" == "0" ]; then
  # Go to the cmt directory and setup cmt
  pushd ../../src/config/cmt > /dev/null
  source CMT_env.sh > /dev/null
  if [ ! -f setup.sh ]; then cmt config > /dev/null 2>&1; fi
  source setup.sh > /dev/null
  if [ "$?" != "0" ]; then
    echo "ERROR! Cannot execute setup.sh"
    exit 1
  fi
  popd > /dev/null
  ccrun=""
fi

# Iterate over the qmr/xml files specified in the input arguments
for thePlatform in ${thePlatforms}; do
  if [ "${thePlatform}" != "" ]; then
    thePlatform=`basename ${thePlatform} .qmr`
    theQmr=${theQmrDir}/${thePlatform}.qmr
    theXml=${theQmrDir}/${thePlatform}.xml
    theSum=${theQmrDir}/${thePlatform}.summary
    # 1. Produce qmtest XML report from QMR
    echo "Produce qmtest XML report from ${theQmr}"
    theQmtDir=
    if [ -f ${theQmr} ]; then
      # See http://unix.stackexchange.com/questions/10826
      # See http://unix.stackexchange.com/questions/92447
      ###theQmrOut=""; while read -n 1 byte; do ord=`LC_CTYPE=C printf '%d' "'$byte"`; if [[ $ord>26 ]] && [[ $ord -lt 127 ]]; then theQmrOut="$theQmrOut$byte"; fi; done < ${theQmr} # test for MacOSX 10.11
      ###echo ${theQmrOut} # test for MacOSX 10.11
      # *NEW!* Use QMTEST_CLASS_PATH if it is in the environment
      # (better strategy than reading it from the qmr, which fails on Ubuntu?)
      if [ "${QMTEST_CLASS_PATH}" != "" ]; then
        theQmtDir=${QMTEST_CLASS_PATH}      
      # *OLD!* (OBSOLETE?) Extract QMTEST_CLASS_PATH from the binary qmr file
      # This is OK on Redhat (1st char after U is encoded path length), 
      # but it is not OK on Ubuntu (path starts after U, 1st char is "/" 
      # if it is a full path) and probably it is not OK on Mac either...
      # Could also try a different strategy matching SYSU...${BINARY_TAG}U?
      else
        theQmtDir=`cat ${theQmr} 2>&1 | awk -v tag="Test.QMTEST_CLASS_PATH" 'BEGIN{for(n=0;n<256;n++)ord[sprintf("%c",n)]=n}{i=index($0,tag); if(i>0){ s=substr($0,i+length(tag)); if(substr(s,1,1)!="U"){print "ERROR!"; exit 1}; n=ord[substr(s,2,1)]; print substr(s,3,n); exit 0 }}'`
        ###theQmtDir=`echo ${theQmrOut} | awk -v tag="Test.QMTEST_CLASS_PATH" 'BEGIN{for(n=0;n<256;n++)ord[sprintf("%c",n)]=n}{i=index($0,tag); if(i>0){ s=substr($0,i+length(tag)); if(substr(s,1,1)!="U"){print "ERROR!"; exit 1}; n=ord[substr(s,2,1)]; print substr(s,3,n); exit 0 }}'` # test for MacOSX 10.11
        if [ "${theQmtDir}" == "" ] || [ "${theQmtDir}" == "ERROR!" ]; then
          echo "WARNING! Could not determine QMTEST_CLASS_PATH from ${theQmr}"
          if [ "${theProjSys}" == "CORALSYS" ]; then
            theQmtDir=`cd ${theQmrDir}/../../src/CoralTest/qmtest; pwd`
          else
            theQmtDir=`cd ${theQmrDir}/../../src/CoolTest/qmtest; pwd`
          fi
        fi
      fi
      if [ ! -d "${theQmtDir}" ]; then
        echo "ERROR! Cannot determine QMTEST_CLASS_PATH:"
        echo "ERROR! Directory ${theQmtDir} does not exist"
        continue
      fi
      echo "Using QMTEST_CLASS_PATH=${theQmtDir}"
      cd $theQmtDir
      # Setup CMake based on CORALSYS/COOLSYS
      if [ "$useCmake" != "0" ]; then
        # *NEW!* Use CORALSYS/COOLSYS if it is in the environment
        # (better strategy than reading it from the qmr, which fails on Ubuntu?)
        if [ "${theProjSys}" == "CORALSYS" ] && [ "${CORALSYS}" != "" ]; then
	  theSysDir=${CORALSYS}  
        elif [ "${theProjSys}" == "COOLSYS" ] && [ "${COOLSYS}" != "" ]; then
	  theSysDir=${COOLSYS}  
        # *OLD!* (OBSOLETE?) Extract CORALSYS/COOLSYS from the binary qmr file
        # This is OK on Redhat (1st char after U is encoded path length), 
        # but it is not OK on Ubuntu (path starts after U, 1st char is "/" 
        # if it is a full path) and probably it is not OK on Mac either...
        # Could also try a different strategy matching SYSU...${BINARY_TAG}U?
        else
          theSysDir=`cat ${theQmr} 2>&1 | awk -v tag="Test.${theProjSys}" 'BEGIN{for(n=0;n<256;n++)ord[sprintf("%c",n)]=n}{i=index($0,tag); if(i>0){ s=substr($0,i+length(tag)); if(substr(s,1,1)!="U"){print "ERROR!"; exit 1}; n=ord[substr(s,2,1)]; print substr(s,3,n); exit 0 }}'`
          ###theSysDir=`echo ${theQmrOut} | awk -v tag="Test.${theProjSys}" 'BEGIN{for(n=0;n<256;n++)ord[sprintf("%c",n)]=n}{i=index($0,tag); if(i>0){ s=substr($0,i+length(tag)); if(substr(s,1,1)!="U"){print "ERROR!"; exit 1}; n=ord[substr(s,2,1)]; print substr(s,3,n); exit 0 }}'` # test for MacOSX 10.11
          if [ "${theSysDir}" == "" ] || [ "${theSysDir}" == "ERROR!" ]; then
            echo "WARNING! Could not determine ${theProjSys} from ${theQmr}"
            theSysDir=`cd ${theQmrDir}/../../${thePlatform}; pwd`
          fi
        fi
        if [ ! -d "${theSysDir}" ]; then
          echo "ERROR! Cannot determine ${theProjSys}:"
          echo "ERROR! Directory ${theSysDir} does not exist"
          continue
        fi
        echo "Using ${theProjSys}=${theSysDir}"
        export ${theProjSys}=${theSysDir}
        if [ "$SUMMARIZE_CCRUN" != "" ]; then
          # (IS THIS NOW OBSOLETE TOO?)
          # Allow summarizing logs from Cmake-based script on another platform
          ccrun="$SUMMARIZE_CCRUN"
        else
          ccrun=`cd ${theSysDir}; pwd`/cc-run
          # (THIS IS VERY OLD, CONCERNS ONLY CMT, AND COULD BE REMOVED!) 
          # Allow summarizing CMT logs from CMake-based script
          # Assume logs are in the build directory at the same level as src
          if [ ! -f ${ccrun} ]; then
            echo "WARNING! File ${ccrun} does not exist"
            ccrun=`cd ${theQmrDir}/../../${BINARY_TAG}; pwd`/cc-run
          fi
        fi
        if [ ! -f ${ccrun} ]; then
          echo "ERROR! File ${ccrun} does not exist"
          continue
        fi
        echo "Using cc-run=${ccrun}"
        ccrun="${ccrun} -q"
      fi
      # Finally produce the XML from the QMR
      \rm -rf ${theXml}
      ### *** NB It is also possible to qmtest report from multiple xxx.qmr ***
      if [ `${ccrun} python -c 'import platform; print (platform.system())'` == "Darwin" ]; then
        ${ccrun} python ${theQmrDir}/myQmtest230 report -o ${theXml} ${theQmr} # CORALCOOL-2884 and CORALCOOL-2883
      else
        ###${ccrun} qmtest report -o ${theXml} ${theQmr} # Fix bug #32789
        ${ccrun} ${theQmrDir}/myQmtest230 report -o ${theXml} ${theQmr}
      fi
      echo "Remove DOCTYPE from the XML report"
      cat ${theXml} | grep -a -v '<\!DOCTYPE' > ${theXml}.new
      \mv ${theXml}.new ${theXml}
      echo "Remove 'Invalid folder node path' from the XML report"
      cat ${theXml} | grep -a -v 'Invalid folder node path' > ${theXml}.new
      \mv ${theXml}.new ${theXml}
      echo "Remove binary characters from the XML report"
      ${ccrun} python ${theQmrDir}/removeBinaryChars.py ${theXml} ${theXml}.new
      \mv ${theXml}.new ${theXml}
    else
      echo "WARNING! File not found: ${theQmr}"
    fi
    # 2. Produce qmtest SUMMARY report from XML
    echo "Produce qmtest summary from ${theXml}"
    if [ -f ${theXml} ]; then
      # Locate QMTEST_CLASS_PATH and CORALSYS/COOLSYS if not already done      
      if [ "$theQmtDir" == "" ]; then
        theQmtDir=`cat ${theXml} | awk -v tag1='Test.QMTEST_CLASS_PATH">&quot;' -v tag2='&quot;</annotation>' '{i1=index($0,tag1); if(i1>0){ s=substr($0,i1+length(tag1)); i2=index(s,tag2); if(i2>0) {print substr(s,1,i2-1); exit 0}; print "ERROR!"; exit 1;}}'`
        if [ "${theQmtDir}" == "" ] || [ "${theQmtDir}" == "ERROR!" ]; then
          echo "WARNING! Could not determine QMTEST_CLASS_PATH from ${theQmr}"
          if [ "${theProjSys}" == "CORALSYS" ]; then
            theQmtDir=`cd ${theQmrDir}/../../src/CoralTest/qmtest; pwd`
          else
            theQmtDir=`cd ${theQmrDir}/../../src/CoolTest/qmtest; pwd`
          fi
        fi
        if [ ! -d "${theQmtDir}" ]; then
          echo "ERROR! Directory ${theQmtDir} does not exist"
          continue
        fi
        echo "Using QMTEST_CLASS_PATH=${theQmtDir}"
        cd $theQmtDir
        # Setup CMake based on CORALSYS/COOLSYS
        if [ "$useCmake" != "0" ]; then
          # *NEW!* Use CORALSYS/COOLSYS if it is in the environment
          # (better strategy than reading it from the xml, that fails on Python3?)
          if [ "${theProjSys}" == "CORALSYS" ] && [ "${CORALSYS}" != "" ]; then
            theSysDir=${CORALSYS}  
          elif [ "${theProjSys}" == "COOLSYS" ] && [ "${COOLSYS}" != "" ]; then
            theSysDir=${COOLSYS}  
          # *OLD!* (OBSOLETE?) Extract CORALSYS/COOLSYS from the xml file
          # This is OK on Python2 (annotation is on one line), but is not ok on
          # Python3 (annotation on 3 lines? why, if qmtest uses Python2 anyway?)
          else
            theSysDir=`cat ${theXml} | awk -v tag1="Test.${theProjSys}\">&quot;" -v tag2='&quot;</annotation>' '{i1=index($0,tag1); if(i1>0){ s=substr($0,i1+length(tag1)); i2=index(s,tag2); if(i2>0) {print substr(s,1,i2-1); exit 0}; print "ERROR!"; exit 1;}}'`
            if [ "${theSysDir}" == "" ] || [ "${theSysDir}" == "ERROR!" ]; then
              echo "WARNING! Could not determine ${theProjSys} from ${theQmr}"
              theSysDir=`cd ${theQmrDir}/../../${thePlatform}; pwd`
            fi
          fi
          if [ ! -d "${theSysDir}" ]; then
            echo "ERROR! Cannot determine ${theProjSys}:"
            echo "ERROR! Directory ${theSysDir} does not exist"
            continue
          fi
          echo "Using ${theProjSys}=${theSysDir}"
          export ${theProjSys}=${theSysDir}
          ccrun=`cd ${theSysDir}; pwd`/cc-run
          # TEMPORARY - START
          # Allow summarizing CMT logs from CMake-based script
          # Assume logs are in the build directory at the same level as src
          if [ ! -f ${ccrun} ]; then
            echo "WARNING! File ${ccrun} does not exist"
            ccrun=`cd ${theQmrDir}/../../${BINARY_TAG}; pwd`/cc-run
          fi
          # TEMPORARY - END
          if [ ! -f ${ccrun} ]; then
            echo "ERROR! File ${ccrun} does not exist"
            continue
          fi
          echo "Using cc-run=${ccrun}"
          ccrun="${ccrun} -q"
        fi
      fi
      # Finally produce the SUMMARY from theXML
      \rm -rf ${theSum}
      ${ccrun} python ${theQmrDir}/parseQmtestRun.py ${theXml} > ${theSum}
      ###export QMTEST_DISABLE_PATCH_TASK18784=1
      ###export QMTEST_DISABLE_PATCH_TASK21088=1
      ###export QMTEST_DISABLE_PATCH_TASK21216=1
      if [ `${ccrun} python -c 'import platform; print (platform.system())'` == "Darwin" ]; then
        ${ccrun} python `${ccrun} which qmtest` summarize --format brief ${theXml} >> ${theSum} # CORALCOOL-2884 and CORALCOOL-2883
      else
        ###${ccrun} qmtest summarize --format batch ${theXml} >> ${theSum}
        ${ccrun} qmtest summarize --format brief ${theXml} >> ${theSum}
        ###${ccrun} qmtest summarize --format full ${theXml} >> ${theSum}
      fi
      cat ${theSum} | grep -v '^Node <DOM Element: result' > ${theSum}.new
      \mv ${theSum}.new ${theSum}
      ###cat ${theSum} | sed "s/CMTCONFIG = ${CMTCONFIG}/CMTCONFIG = ${thePlatform}/" > ${theSum}.new
      ###\mv ${theSum}.new ${theSum}
      echo "Produced qmtest summary ${theSum}"
      echo "=============================================================================="
      tail ${theSum}
      echo "=============================================================================="
    else
      echo "ERROR! File not found: ${theXml}"
    fi
    echo "Done!"
  fi
done

